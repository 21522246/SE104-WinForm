﻿using BookShop_Management.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookShop_Management.UserControls
{
    public partial class LichSuBanSach : UserControl
    {
        DataTable DS_LichSuBanSach;

        #region Methods

        public LichSuBanSach()
        {
            InitializeComponent();

            label_LichSuBanSach.Text = Variables.label_LichSuBanSach;

            LoadData();
        }

        private void LoadData()
        {
            DS_LichSuBanSach = HoaDonDAO.Instance.LayDS_HD();

            Handle_Data();

            dataGridView_LichSuBanSach_Fill.DataSource = DS_LichSuBanSach;
        }


        private void Handle_Data()
        {
            DS_LichSuBanSach.Columns.Add("STT", typeof(int));

            int i = 1;
            foreach (DataRow dr in DS_LichSuBanSach.Rows)
                dr["STT"] = (i++).ToString();


            DS_LichSuBanSach.Columns["STT"].SetOrdinal(0);
            DS_LichSuBanSach.Columns.Remove("GiamGia");

            // thay đổi column name
            if (DS_LichSuBanSach.Columns["MaHD"] != null)
                DS_LichSuBanSach.Columns["MaHD"].ColumnName = "Mã HD";
            if (DS_LichSuBanSach.Columns["NgayHD"] != null)
                DS_LichSuBanSach.Columns["NgayHD"].ColumnName = "Ngày HD";
            if (DS_LichSuBanSach.Columns["TongHoaDon"] != null)
                DS_LichSuBanSach.Columns["TongHoaDon"].ColumnName = "Tổng HD";
            if (DS_LichSuBanSach.Columns["SoTienDaThanhToan"] != null)
                DS_LichSuBanSach.Columns["SoTienDaThanhToan"].ColumnName = "Số Tiền Đã Thanh Toán";
            if (DS_LichSuBanSach.Columns["MaNguoiDung"] != null)
                DS_LichSuBanSach.Columns["MaNguoiDung"].ColumnName = "Mã ND";

            dataGridView_LichSuBanSach_Fill.CellFormatting += dataGridView_LichSuBanSach_Fill_CellFormatting;
            
        }
        #endregion

        #region Events

        //format money
        private void dataGridView_LichSuBanSach_Fill_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dataGridView_LichSuBanSach_Fill.Columns[e.ColumnIndex].Name == "Tổng HD"
                || this.dataGridView_LichSuBanSach_Fill.Columns[e.ColumnIndex].Name == "Số Tiền Đã Thanh Toán")
            {
                if (e.Value != null)
                {
                    try
                    {
                        e.Value = String.Format("{0:# ### ###}", e.Value);
                        e.FormattingApplied = true;
                    }
                    catch (FormatException)
                    {
                        e.FormattingApplied = false;
                    }
                }
            }
        }

        private void textBox_TraCuu_TextChanged(object sender, EventArgs e)
        {
            if (textBox_TraCuu.Text == "")
                dataGridView_LichSuBanSach_Fill.DataSource = DS_LichSuBanSach;
            else
            {
                DataRow[] data = DS_LichSuBanSach.Select(string.Format("MaKH like '%{0}%'",
                    textBox_TraCuu.Text));

                DataTable temp = DS_LichSuBanSach.Clone();
                foreach (DataRow dr in data)
                    temp.Rows.Add(dr.ItemArray);

                dataGridView_LichSuBanSach_Fill.DataSource = temp;
            }
        }

        #endregion

    }
}
